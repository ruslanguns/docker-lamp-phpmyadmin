FROM php:7.3-apache-stretch
LABEL maintainer="Ruslan Gonzalez <ruslanguns@gmail.com>"

# VARIABLES DE ENTORNO
ARG ROOT_PASSWORD=rusgunx

ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get clean

RUN apt-get update && apt-get install -y \
    apt-utils \
    openssh-server \
    git \
    nano \
    wget \
    zip \
    unzip \
    cron \
    curl \
    openssl \
    openssh-server \
    libjpeg62-turbo-dev \
    libpng-dev \
    patch \
    libzip-dev \
    libxml2-dev \
    libcurl3-dev \
    libc-client-dev libkrb5-dev && rm -r /var/lib/apt/lists/*

RUN a2enmod rewrite
RUN service apache2 restart

#######################
# Docker instalacion de todas librerias comunes
#######################
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
	&& echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini
RUN docker-php-ext-install imap zip soap sockets mbstring curl ftp pdo tokenizer session

	
#######################
# FIN instalacion de todas librerias comunes 
#######################

#######################
# Obteniendo el php.ini
#######################
RUN mkdir /usr/src/php
RUN tar --file /usr/src/php.tar.xz --extract --strip-components=1 --directory /usr/src/php
RUN cp /usr/src/php/php.ini-production /usr/local/etc/php/php.ini

#######################
# CONFIGURANDO EL ACCESO POR SSH
#######################
RUN mkdir /var/run/sshd
RUN echo 'root:${ROOT_PASSWORD}' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd


ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

RUN rm -rf /etc/apache2/sites-available/* && rm -rf /etc/apache2/sites-enabled/*
EXPOSE 22 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 3306

ADD config/php.ini /usr/local/etc/php
ADD config/apache2.conf /etc/apache2/apache2.conf
COPY config/apache-ports.conf /etc/apache2/ports.conf
COPY config/apache-sites/* /etc/apache2/sites-available/

RUN cd /etc/apache2/sites-available
RUN a2ensite *

#######################################
# ARRANCAMOS EL SCRIPT DE CONFIGURACION
#######################################
COPY run.sh /run.sh
RUN chmod +x /run.sh
CMD ["/bin/bash", "-lc", "/run.sh"]