# Docker LAMP for MultiApps Hosting

## Introduction

This Docker configuration is HUGE, and yes so heavy for simple things I just wrapped all necesary components into the Dockerfile to just have everything that a PHP Project requires in the server side for mailing, reporting, etc. Please feel free to adjust the Dockerfile to your needs, anyways this container has been tested and is fine for production for low sizes applications, for large consider using a dedicated Dockerfile configuration nor this image has been tested in 100+ concurrent users.

## Preparation

Please setup the environment variables for the stack:

In linux:

```bash
`export MYSQL_ROOT_PASSWORD="YOUR_PASSWORD"`
`export ROOT_PASSWORD="YOUR_PASSWORD"`
```

Or just for instance run the Variable value together with the command of docker:

```bash
MYSQL_ROOT_PASSWORD="YOUR_PASSWORD" ROOT_PASSWORD="YOUR_PASSWORD" docker-compose up -d
```

## For production:

MySQL, PhpMyAdmin, Apache2, PHP 7.2.17

```bash
docker-compose up -d
```

## For Development

Download / Fork and start coding!

Keep in mind that there are some folders that contains the apache settings and the environment for extending or reduce the configuration file.
